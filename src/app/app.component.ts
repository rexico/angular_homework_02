import { createInjectorType } from '@angular/compiler/src/render3/r3_injector_compiler';
import { Component, HostBinding, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from './common/interfaces/city.interface';
import { Current } from './common/interfaces/current.interface';
import { Hourly } from './common/interfaces/hourly.interface';
import { JsonPlaceholderService } from './common/services/json-placeholder.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'homework02';
  city: City = {name: 'Dnipro, ua', lat: 48.45, lon: 34.9833};
  // city: City = {name: 'London, uk', lat: 51.5085, lon: 0.12574};
  current$!: Observable<Current>;
  hourly$!: Observable<Hourly[]>;

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  constructor (private readonly jsPlaceholderService: JsonPlaceholderService) {}
  
  ngOnInit(): void {
    
    this.current$ = this.jsPlaceholderService.getCurrentWeather(this.city);
    this.hourly$ = this.jsPlaceholderService.getHourlyForecast(this.city);

    console.log(this.current$);
    this.current$.subscribe(c=>console.log(c));

    console.log(this.hourly$);
    this.hourly$.subscribe(c=>console.log(c));

  }

  
}
