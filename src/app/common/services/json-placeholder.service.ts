import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@app-environments/environment";
import { Observable } from "rxjs";
import { City } from "../interfaces/city.interface";
import { Current } from "../interfaces/current.interface";
import { map } from "rxjs/operators";
import { Hourly } from "../interfaces/hourly.interface";

@Injectable()
export class JsonPlaceholderService {
    constructor (private readonly httpClient: HttpClient) {}

    getCurrentWeather(city: City): Observable<Current> {
        return this.httpClient.get([environment.API_URL, '/data/2.5/onecall'].join('')
                        , {params: {lat: city.lat, lon: city.lon, units: 'metric', lang: 'ru', APPID: environment.APPID}})
                        .pipe(map((r: any)=>r.current));
    }
    getHourlyForecast (city: City): Observable<Hourly[]>{
        return this.httpClient.get([environment.API_URL, '/data/2.5/onecall'].join('')
                        , {params: {lat: city.lat, lon: city.lon, units: 'metric', lang: 'ru', APPID: environment.APPID}})
                        .pipe(map((r: any)=>r.hourly));
    }
}